** Ubercart custom 'Review Order'

** What is it?
This module lets you customise the 'Review order' button on the Ubercart checkout page.
Usability testing on Ubercart checkout concluded that the 'Review order' button text during checkout was confusing, 
and in every site we made with UC we ended up changing the text to read 'Continue'. After a while this became u manageable
so we developed a simple module to allow admins to change the button's value to their desire. This module allows admins to
update the checkout to make it more user friendly but at the same time allows for updating of Ubercart core files without
the need to remember to update the button's value every time.

** Access options
Permissions are available to let specific roles edit the button

** Installation
Upload and enable, that's it!

** Credits
Module developed by Harry Edwards May 2011